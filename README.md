# Software-Testing-Project-Scope-Blockchain

**Important Commands**

- `truffle migrate`
- `truffle test`


**Important Information Related to Project**

- This is a dApp (decentralized application) on the Ethereum Blockchain which serves as Full-Stack a Decentralized Voting System
- Truffle framework is being employed to build dApp on Eth Blockchain
- Ganache is used as the local in-memory blockchain.
- Metamask to connect to our local Ethereum blockchain with our personal account, and interact with our smart contract
- For Testing we are using [Mocha](https://mochajs.org/) and [Chai](http://www.chaijs.com/)

Following are the results of the testing:
-         ✔ initializes with two candidates (141ms)
-         ✔ it initializes the candidates with the correct values (324ms)
-         ✔ allows a voter to cast a vote (1034ms)
-         ✔ throws an exception for invalid candidates (958ms)


    4 passing (3s)
